import React from 'react';
import { Provider } from 'react-redux';
import Main from './src/Main';
import store from './src/reducers/store';




const  App:React.FC = () => {
  return (
    <Provider store={store}>
      <Main/>        
    </Provider>
  );
}

export default App;