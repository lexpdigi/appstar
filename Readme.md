Instruções para execução do Projeto:
clone o projeto deste repo

Apos entre na Pasta do app e realize os seguintes comandos

1 - npm install - para instalar as dependências do projeto
2 - npx react-native start - para iniciar o metro bundler
3 - npx react-native run-android --no-jetifier - para iniciar a aplicação
4 - Para logar entre com as credenciais 
Email  - lexpdigi@gmail.com
Password - 123456

Informações Adicionais

Node - v16.11.0
Npm -  8.0.0

Obs:Caso de algum erro na compilação entre na pasta android
e execute o seguinte comando:
gradlew installDebug