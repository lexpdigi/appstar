import React,{useCallback, useImperativeHandle, useState,forwardRef, ForwardedRef } from 'react';
import { StyleSheet,View,Modal, Image,Text, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { setSignedAction } from '../../actions/userAction';
import authService from '../../database/service/auth.service';
import { selectSigned, selectCurrentUser } from '../../selectors/auth.selector';

const img  = require('../../images/Jobs.png');

export interface ModalHandlers {
    handleOpenModal:()=>void,
    handleCloseModal:()=>void
}

interface ModalProps{
    onPress:()=>void
}


 export const  ModalX = forwardRef<ModalHandlers,ModalProps>(
    
 (props:any,ref:ForwardedRef<ModalHandlers>) => {

        const signed  = useSelector(selectSigned);
        const currentUser  = useSelector(selectCurrentUser);
        const dispatch = useDispatch();

        const [open,setOpen]= useState(true);

        const handleOpenModal = useCallback(()=>{
            setOpen(true);
        },[]) 

        const handleCloseModal = useCallback(()=>{
            setOpen(false);
        },[]) 

        useImperativeHandle(ref,()=>{
            return {
                handleOpenModal,
                handleCloseModal
            };
        });

        const setSigned = async() =>{
           let resp = await authService.updateSigned(true,currentUser.id);
           dispatch(setSignedAction(true))
           props.onPress();
        }

    
     

        return (
            <View ref={ref} {...props}>
                <Modal
                animationType={'fade'}
                visible={open}
                transparent={true}
                >
                    <View style={styles.modal}>
                        <View style={styles.row}>
                            <Text onPress={handleCloseModal} style={styles.textClose}>&times;</Text>
                        </View>
                        <View style={styles.containerText}>
                            <Text style={styles.title}>Faça upgrade no</Text>
                            <Text style={styles.title}>Jurishand e seja feliz.</Text>
                        </View>
                        <View style={styles.containerImg}>
                            <Image source={img} />
                        </View>
                        <View style={[styles.containerText,styles.bottom_20]}>
                            <Text style={styles.subTitle}>Assine por R$299,90 por ano</Text>
                            <Text style={styles.subTitle}>(R$24,90 por mês)</Text>
                        </View>
                        <TouchableOpacity style={[styles.btnSign,(signed)?styles.disabled:{}]} disabled={signed} onPress={()=> setSigned()}>
                            <Text style={styles.centerText}>Assine agora</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            
            </View>
        );
    }
 );
 
 

const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        justifyContent:'flex-end',
    },
    containerText:{
        flexDirection:'column',
    },
    containerImg:{
        flexDirection:'row',
        justifyContent:'center'
    },
    title:{
        fontSize:30,
        color:'#43505c',
        textAlign:'center'
    },
    subTitle:{
        fontSize:20,
        color:'#43505c',
        textAlign:'center'
    },
    bottom_20:{
        paddingBottom:20
    },
    modal:{
        flexDirection:'column',
        backgroundColor:'#e2e6ea',
        borderRadius:20,
        padding:20,
        margin:40,
        marginTop:120,
        elevation:10,
        justifyContent:'flex-end'
    },
    centerText:{
        textAlign:'center',
        fontSize:20,
        color:'#fC6CAd',
    },
    textClose:{
        fontSize:40,
        color:'#7868fd',
        fontWeight:'bold',
    },
    btnSign:{
        flexDirection:'column',
        backgroundColor:'#f6f6f9',
        padding:20,
        borderRadius:10,
        textAlign:'center',
        elevation:1,
    },
    disabled:{
        backgroundColor:'#ddd'
    }
})
