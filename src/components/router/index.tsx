import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginX from '../../screens/login';


const Stack = createNativeStackNavigator();

const options = {
  headerShown:false
};

const  RouterX:React.FC = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={LoginX} options={options} />
    </Stack.Navigator>
  );
}

export default RouterX;