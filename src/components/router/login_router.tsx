import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Update from '../../screens/update';
import SettingsX from '../../screens/settings';


const Stack = createNativeStackNavigator();

const options = {
  headerShown:false
};

const  AuthRouter:React.FC = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Settings" component={SettingsX} options={options} />
      <Stack.Screen name="Update" component={Update} options={options} />
    </Stack.Navigator>
  );
}

export default AuthRouter;