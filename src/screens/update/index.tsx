import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import {ModalX} from '../../components/modal/index';
import Icon from 'react-native-vector-icons/MaterialIcons';



const  Update:React.FC = ({navigation}) => {

  const goSettings = () => {
    navigation.push('Settings');
  }
  

  return (
    <View style={styles.container}>  
        <Icon name={'arrow-back'} style={[styles.left_18]} size={40} color={'#7666fc'} onPress={()=>goSettings()}/>
        <ModalX  onPress={goSettings}/>
        <View style={styles.containerTrial}>
          <Text style={styles.textTrial}> Você será cobrado imediatamente</Text>
          <Text style={styles.textTrial}> Sem período trial</Text>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    flexDirection:'column',
    justifyContent:'space-between',
    flex:1
  },
  containerTrial:{
    marginBottom:20
  },
  left_18:{
    paddingLeft:18
  },
  textTrial:{
    color:'#62707E',
    textAlign:'center',
    fontSize:16
  }
})

export default Update;