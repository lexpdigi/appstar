import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TextInput, Text, TouchableOpacity,Alert } from 'react-native';
import { useDispatch } from 'react-redux';
import { setCurrentUserSuccess } from '../../actions/userAction';
import authService from '../../database/service/auth.service';
import SQLiteManager from '../../database/SQLiteManager';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const  LoginX:React.FC = () => {

    const [showPwd,setShowPwd]  = useState(true);
    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const dispatch = useDispatch();

    useEffect(()=>{
        SQLiteManager.initDB()
    },[]);

    const validPassword = (pwd)=>{
        return pwd.length >= 6;
    }

    const _login = async () =>{
        
        if(email && password){

            if(!validPassword(password)){
                Alert.alert('Aviso!','O password deve ter ao menos 6 caracteres!')
                return  false;
            }

            try{
                let user =  await authService.login(email,password);
                dispatch(setCurrentUserSuccess(user))
            }catch(err){
                Alert.alert('Aviso!','Credenciais Inválidas!')
            }
          
       
        }else{
            Alert.alert('Aviso!','Preencha os campos!')
            return  false;
        }
    }


  

  return (
    <View style={styles.container}>  
        <Text style={styles.title}>Juris Hand App</Text>
        <TextInput placeholder={'Email'} style={[styles.input,styles.textInput]} value={email} onChangeText={(val)=>setEmail(val)}/>
        <View style={[styles.containerPwd,styles.input]}>
          <TextInput secureTextEntry={showPwd} placeholder={'Password'} style={styles.textInput} value={password}  onChangeText={(val)=>setPassword(val)}/>
          {
            (showPwd)?(
              <Icon name={'eye'} color={'#000'} size={40} style={styles.top_10} onPress={()=>setShowPwd(false)}/>
            ):(
              <Icon name={'eye-off'} color={'#000'} size={40} style={styles.top_10} onPress={()=>setShowPwd(true)}/>
            )
          }
        </View>
        <TouchableOpacity style={styles.loginBtn} onPress={()=>_login()}>
            <Text style={styles.centerText}>Login</Text>
        </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    flexDirection:'column',
    flex:1,
    marginTop:280
  },
  left_18:{
    paddingLeft:18
  },
  top_10:{
    marginTop:10
  },
  containerPwd:{
    flexDirection:'row',
    justifyContent:'space-between',
  },
  input:{
      height:60,
      backgroundColor:'#ddd',
      paddingHorizontal:10,
      margin:20,
      borderRadius:50
  },
  textInput:{
    fontSize:20,
    color:'#000',
  },
  centerText:{
    textAlign:'center',
    fontSize:20,
    color:'#000',
},
  loginBtn:{
    flexDirection:'column',
    backgroundColor:'#00deee',
    padding:20,
    borderRadius:15,
    textAlign:'center',
    elevation:1,
    margin:30
  },
  title:{
      fontSize:40,
      textAlign:'center'
  }
})

export default LoginX;