import React,{useEffect} from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import SQLiteManager from '../../database/SQLiteManager';


const imgIconSettings = require('../../images/update-arrows.png');


const  SettingsX:React.FC = ({navigation}) => {


   useEffect(()=>{
    SQLiteManager.initDB()
  },[])
  
  

  const goUpdate = () => {
    navigation.push('Update');
    
  }

  return (
    <View style={styles.container}>  
      <Text style={[styles.title,styles.left_25]}>Configurações</Text>
      <View style={[styles.row,styles.left_15,styles.top_15]}>
          <TouchableOpacity onPress={()=>goUpdate()}>
            <Image style={styles.right_15} source={imgIconSettings}/>
          </TouchableOpacity>
          <Text onPress={()=>goUpdate()}>Faça upgrade agora</Text>
      </View>
    </View>
  );
}



const styles = StyleSheet.create({
 top_15:{
        marginTop:15
  },
  row:{
      flexDirection:'row',
      justifyContent:'flex-start'
  },  
  right_15:{
      marginRight:15
  },
  left_15:{
    marginLeft:15
  },
  container:{
    flexDirection:'column'
  },
  title:{
      fontSize:30,
      color:'#43505c'
  },
  left_25:{
      paddingLeft:25
  },
  left_18:{
      paddingLeft:18
  }
})

export default SettingsX;