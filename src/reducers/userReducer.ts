import { Reducer } from "redux";
import { UserState, AUTH_TYPES } from '../actions/types';

const INITIAL_STATE:UserState = {
    err:'',
    currentUser:null,
    signed:true
}

export const userReducer:Reducer<UserState> = (state = INITIAL_STATE,action)=>{

    switch(action.type){
        case AUTH_TYPES.SET_CURRENT_USER_SUCCESS:
            let valSigned = (action.payload.signed == 0)?false:true;
            return { ... state,currentUser:action.payload,signed:valSigned,err:''}
        break;
        case AUTH_TYPES.SET_CURRENT_USER_FAILURE:
            return { ... state,currentUser:null,signed:false,err:action.payload}
        break;
        case AUTH_TYPES.SET_SIGNED:
            let valSignedUpdate = (action.payload.signed == 0)?false:true;
            return { ... state,signed:valSignedUpdate,err:''}
        break;
        case AUTH_TYPES.SET_LOGOUT:
            return { ... state,currentUser:null,signed:false,err:''}
        break;
        default:
            return state;
    }

}

export default userReducer;