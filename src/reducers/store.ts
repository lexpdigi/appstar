import {createStore,applyMiddleware, Store} from 'redux'
import { rootReducer } from './rootReducer';
import { UserState } from '../actions/types';



const middleWares = [];


interface ApplicationState{
    auth:UserState
}



export const store:Store<ApplicationState>  = createStore(rootReducer,applyMiddleware(...middleWares));



export default  store;