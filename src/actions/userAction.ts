import { AUTH_TYPES } from './types';
import {action} from 'typesafe-actions';


const setCurrentUserSuccess = (user)=>action(AUTH_TYPES.SET_CURRENT_USER_SUCCESS,user)



const setCurrentUserFailure = (err)=>  action(AUTH_TYPES.SET_CURRENT_USER_FAILURE,err.message)

const setSignedAction = (signed)=>  action(AUTH_TYPES.SET_SIGNED,signed)

const setLogOut = ()=>  action(AUTH_TYPES.SET_LOGOUT)



export {setCurrentUserFailure,setCurrentUserSuccess,setSignedAction,setLogOut}