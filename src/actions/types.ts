export enum AUTH_TYPES {
    SET_CURRENT_USER_SUCCESS = 'SET_CURRENT_USER_SUCCESS',
    SET_CURRENT_USER_FAILURE = 'SET_CURRENT_USER_FAILURE',
    SET_SIGNED = 'SET_SIGNED',

    SET_LOGOUT = 'SET_LOGOUT',
}

export interface UserState{
    err:string,
    currentUser:any,
    signed:boolean
}