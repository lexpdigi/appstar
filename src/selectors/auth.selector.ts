
import {createSelector} from 'reselect';

const selectAuth = state => state.auth;

export const selectCurrentUser = createSelector(
    [selectAuth],
    (auth)=>auth.currentUser
)


export const selectSigned = createSelector(
    [selectAuth],
    (auth)=>auth.signed
)

export const selectErrors = createSelector(
    [selectAuth],
    (auth)=>auth.err
)
