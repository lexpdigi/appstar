export const SupportedTypes = {
    INTEGER: {
        value: 'INTEGER',
        type: 'INTEGER',
        default_value: null
    },
    LONG: {
        value: 'LONG',
        type: 'INTEGER',
        default_value: null
    },
    DOUBLE: {
        value: 'DOUBLE',
        type: 'REAL',
        default_value: null
    },
    TEXT: {
        value: 'TEXT',
        type: 'TEXT',
        default_value: null
    },
    BOOLEAN: {
        value: 'BOOLEAN',
        type: 'INTEGER',
        default_value: null
    },
    DATETIME: {
        value: 'DATETIME',
        type: 'DATETIME',
        default_value: 'CURRENT_TIMESTAMP'
    },
    SYNC_STATUS: {
        value: 'STATUS',
        type: 'TEXT',
        default_value: null
    },
    JSON: {
        value: 'JSON',
        type: 'TEXT',
        default_value: null
    },
};

export const Tables = {
    User: {
        id: {
            type: SupportedTypes.INTEGER,
            primary_key: true,
            auto_increment:true
        },
        signed:{
            type:SupportedTypes.BOOLEAN,
            default_value:0
        },
        name: {
            type: SupportedTypes.TEXT,
            primary_key: false,
            default_value: null,
        },
        email: {
            type: SupportedTypes.TEXT,
            primary_key: false,
            default_value: null,
        },
        password: {
            type: SupportedTypes.TEXT,
            primary_key: false,
            default_value: null,
        },
        created_at: {
            type: SupportedTypes.DATETIME,
            primary_key: false,
            default_value:'CURRENT_TIMESTAMP'
        },
        updated_at: {
            type: SupportedTypes.DATETIME,
            primary_key: false,
            default_value:'CURRENT_TIMESTAMP'
        },
        
    },
};
