

class UserService{
   getSqlInsert(){
       return `INSERT INTO user(name,email,password) VALUES(?,?,?)`;
   }

   getSqlSelect(){
       return `SELECT * FROM user `;
   }

   getSqlSelectByEmail(){
    return `SELECT * FROM user WHERE email = ?`;
   }

   getSqlTableExists(){
       return `SELECT 1 FROM user LIMIT 1`;
   }

   getSqlUpdate(){
       return `UPDATE user SET signed = ? WHERE id = ?`;
   }
}

export default new UserService();