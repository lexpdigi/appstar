import { openDatabase } from "react-native-sqlite-storage";
import { database_name } from "../SQLiteManager";
import userService from "./user.service";
import BcryptReactNative from 'bcrypt-react-native';


class AuthService{
    async login(email,password){
        let user = null;
        let db = await openDatabase({
            name:database_name
        })

        return new Promise((resolve,reject)=>{
            
            db.executeSql(userService.getSqlSelectByEmail(),[email])
            .then(async (val) => {
                console.log('login success')

                let len = val[0]['rows'].length;


                if(len > 0){
                    let results = [];

                    for(let cont = 0;cont < len;cont++){
                        let item = val[0]['rows'].item(cont);

                        results.push(item)
                        
                    }

                    let isSamePassword = await BcryptReactNative.compareSync(password,results[0].password);

                    if(results.length > 0){
                        if(isSamePassword){
                            user = results[0]
                        }
                    }
                }

              if(user){
                resolve(user);
              }else{
                  reject(user)
              }
                
            })
            .catch((error) => {
                console.log('error login')
                console.log(error)
                reject(error);
            });
        })

    }

    async updateSigned(signed,id){
        let res = null;
        let db = await openDatabase({
            name:database_name
        })

        return new Promise((resolve,reject)=>{
            
            db.executeSql(userService.getSqlUpdate(),[signed,id])
            .then( (val) => {
                console.log('update signed success')
                resolve()
            })
            .catch((error) => {
                console.log('error update signed')
                console.log(error)
                reject(error);
            });
        })

    }
}


export default new AuthService();