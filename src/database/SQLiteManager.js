import SQLite from 'react-native-sqlite-storage';
import BcryptReactNative from 'bcrypt-react-native';


SQLite.DEBUG(true);
SQLite.enablePromise(true);

import * as schema from './schemas';
import userService from './service/user.service';

export const database_name = 'teste.db';


const user = {
    email:'lexpdigi@gmail.com',
    password:'123456',
    name:'Alex P'
}

class SQLiteManager {
    
    
    initDB() {
        let db;
                SQLite.openDatabase(
                        {
                            name:database_name
                        }
                    )
                    .then((DB) => {
                        this.db = DB;
                        db = DB;
                        db.executeSql(userService.getSqlSelect())
                            .then((val) => {
                                console.log('sucess')                            
                            })
                            .catch((error) => {
                                console.log('errr')
                                db.transaction((tx) => {
                                        for (const name in schema.Tables) {
                                            this.createTable(tx, schema.Tables[name], name);
                                        }
                                    })
                                    .then(async () => {

                                        const salt = await BcryptReactNative.getSalt(10);
                                        const hash = await BcryptReactNative.hash(salt,user.password);
                                        
                                        this.db.transaction(tx =>{
                                            tx.executeSql(
                                                userService.getSqlInsert(),[
                                                    user.name,
                                                    user.email,
                                                    hash
                                                ],
                                                (val) => {
                                                    console.log('sucess insert')
                                                },
                                                (err) => {
                                                    console.log('error insert')
                                                    console.log(err)
                                                },
                                            );
                                        });
                                    })
                            });

                        resolve(db);
                    })
                    .catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
            
    }

    closeDatabase(db) {
        if (db) {
            db.close()
                .then((status) => {
                    console.log('sucess close db')
                })
                .catch((error) => {
                    console.log('error close db')
                });
        } 
    }

   

    createTablesFromSchema() {
        if (this.db) {
            this.db.transaction((tx) => {
                for (const name in schema.Tables) {
                    this.createTable(tx, schema.Tables[name], name);
                }
            });
        }
    }

    dropDatabase() {
        return new Promise((resolve, reject) => {
            SQLite.deleteDatabase(database_name)
                .then(() => {
                    SQLite.openDatabase(
                        {
                            name:database_name
                        }
                    );
                })
                .then(() => {
                    resolve();
                })
                .catch((err) => {
                    reject(err);
                });
        })
    }

    createTable(tx, table, tableName) {
        let sql = `CREATE TABLE IF NOT EXISTS ${tableName.toLowerCase()} `;
        const createColumns = [];
        for (const key in table) {
            createColumns.push(
                `${key} ${table[key].type.type} ${
          table[key].primary_key ? 'PRIMARY KEY NOT NULL ' : ''
        } default ${table[key].default_value}`,
            );
        }
        sql += `(${createColumns.join(', ')});`;
        tx.executeSql(
            sql,
            [],
            () => {
                console.log('sucess exec sql')
            },
            () => {
                console.log('error exec sql')
            },
        );
    }
}

export default new SQLiteManager();
