


import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar,StyleSheet,View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import AuthRouter from './components/router/login_router';
import RouterX from './components/router';
import { selectCurrentUser } from './selectors/auth.selector';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { setLogOut } from './actions/userAction';




const  Main:React.FC = () => {
  const currentUser = useSelector(selectCurrentUser);
  const dispacth = useDispatch();

  const signOut = ()=> dispacth(setLogOut());

  return (
    
    <NavigationContainer>
      <StatusBar backgroundColor={'#f7f7f7'}  barStyle={'dark-content'} />
      {
        (currentUser)?(
          <View style={styles.rowContainer}>
            <Icon name={'logout'}   size={40} color={'#7666fc'} onPress={()=>signOut()}/>
          </View>
        ):null
      }
      {
          (currentUser)?(
                <AuthRouter/>
          ):(
            
          <RouterX/>
          )
      }
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  rowContainer:{
    flexDirection:'row',
    justifyContent:'flex-end',
    paddingRight:10,
    backgroundColor:'#f7f7f7'
  }
})

export default Main;
